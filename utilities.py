import unittest

## Couple notes
# A row is a list of values of either (empty, filled, not_filled) (0, 1, 2)
# A row will be presented as a python list


##
# untested utilities
##

# intify a list of lists of strings that have integers in them
def intify_lol(l: list):
    li = []

    for i in l:
        li_inner = []
        for j in i:
            li_inner.append(int(j))
        li.append(li_inner)

    return li

# intify a list of characters of integers
def intify_list(l: list):
    return [int(i) for i in l]

def get_row_answer_split_on_2(row: list) -> list:
    row_string = ''.join(row)
    row_list = row_string.split('2')
    row_item = []
    for j in row_list:
        row_item.append(len(j))
    row_item = list(filter(lambda k: k != 0, row_item))

    if len(row_item) == 0:
        row_item = [0]

    return row_item

def get_row_answer(row: list) -> list:
    row_string = ''.join(str(row))
    row_list = row_string.split('0')
    row_item = []
    for j in row_list:
        row_item.append(len(j))
    row_item = list(filter(lambda k: k > 0, row_item))

    return row_item

# Get the row and column scores for a matrix of points
def get_answers(rows: list) -> list:
    # Get a list of lists of columns from the rows
    columns = list(zip(*rows))
    row_scores = []
    column_scores = []

    # Score the rows
    for row in rows:
        row_scores.append(get_row_answer(row))

    # Score the columns
    for column in columns:
        column_scores.append(get_row_answer(column))

    return row_scores, column_scores

# Print the answers given that the list of answers looks like: [[row, row], [column, column]]
# Will print the result of the get_answers() function
def print_answers(answers: list):
    print("             ", end="")

    # Print columns first
    for i in answers[1]:
        print(i, end=" ")

    print("")

    # Print rows second
    for i in answers[0]:
        print(i)




##
# unit tested section
##




# A pattern is a list of numbers like: [1, 3, 4]
# The return list is a list of numbers, which represent 1's for each pattern element in a row, separated by 2's
# the row_size will fill in 2's for the leftover
# so ([1, 3, 4], 10) turns into: [1, 2, 1, 1, 1, 2, 1, 1, 1, 1]
# and ([1, 3], 10) turns into: [1, 2, 1, 1, 1, 2, 2, 2, 2, 2]
def starting_row(pattern: list, row_size: int) -> list:
    result = []

    for i in pattern:
        for i in range(0, i):
            result.append(1)

        if len(result) < row_size:
            result.append(2)

    difference = row_size - len(result)
    for i in range(0, difference):
        result.append(2)

    return result


# Should return the 'blockified' row
# [1, 2, 2, 1, 1] -> [1, 0, 0, 2] to show that
# there is 1 input in a row, followed by 2 empties, then 2 in a row
def to_block(row: list) -> list:
    # If the row is ultimately empty, just leave
    if len(row) == 0:
        return []

    result = []

    # will keep track of how many 1's in a row we see
    count = 0

    for i in row:
        if i == 1:
            count = count + 1

        elif i == 2:
            if count > 0:
                result.append(count)
            result.append(0)
            count = 0

    # If the list ends with a 1, we haven't appended yet
    if count > 0:
        result.append(count)

    return result


# Should result the normal row given a block row
# [1, 0, 0, 2] -> [1, 2, 2, 1, 1]
def to_row(block_row: list) -> list:
    result = []

    if len(block_row) == 0:
        return []

    for i in block_row:
        if i == 0:
            result.append(2)

        else:
            for _ in range(0, i):
                result.append(1)

    return result

# Increments each block to the next empty space
def fuzz_row(block_row: list) -> list:
    if len(block_row) == 0:
        return block_row
    if block_row[-1] != 0:
        # We are done
        return block_row

    ret = block_row.copy()

    # moves the last element in the list to the beginning of the list
    ret.insert(0, ret.pop())

    return ret

# def get_common_pattern(fuzzed_rows: list) -> list:
#     if len(fuzzed_rows) == 0: return []
#     common_row = []

#     common_row = fuzzed_rows[0].copy()

#     for row in fuzzed_rows:
#         for i, element in enumerate(row):
#             if element != common_row[i]:
#                 common_row[i] = 0

#     return common_row


# def find_commonality(pattern: list, size: int) -> list:
#     if size == 0: return []

#     row = starting_row(pattern, size)
#     list_of_rows = []

#     while (True):
#         last_row = row.copy()
#         list_of_rows.append(last_row)
#         row = to_row(fuzz_row(to_block(row)))
#         if last_row == row:
#             break

#     common = get_common_pattern(list_of_rows)

#     return common


##
#  Testing section
##

# class TestFindCommonality(unittest.TestCase):
#     def test_find_commonality_emtpy_1(self):
#         self.assertEqual([], find_commonality([], 0))

#     def test_find_commonality_fully_filled_1(self):
#         self.assertEqual([1], find_commonality([1], 1))

#     def test_find_commonality_52(self):
#         self.assertEqual([0, 0, 1, 1, 1, 0, 0, 0, 0, 0], find_commonality([5, 2], 10))

#     def test_find_commonality_123(self):
#         self.assertEqual([0, 0, 0, 0, 0, 0, 0, 1, 0, 0], find_commonality([1, 2, 3], 10))

#     def test_find_commonality_6(self):
#         self.assertEqual([0, 0, 0, 0, 1, 1, 0, 0, 0, 0], find_commonality([6], 10))

#     def test_find_commonality_7(self):
#         self.assertEqual([0, 0, 0, 1, 1, 1, 1, 0, 0, 0], find_commonality([7], 10))

#     def test_find_commonality_10(self):
#         self.assertEqual([1, 1, 1, 1, 1, 1, 1, 1, 1, 1], find_commonality([10], 10))

#     def test_find_commonality_36(self):
#         self.assertEqual([1, 1, 1, 2, 1, 1, 1, 1, 1, 1], find_commonality([3, 6], 10))

#     def test_find_commonality_18(self):
#         self.assertEqual([1, 2, 1, 1, 1, 1, 1, 1, 1, 1], find_commonality([1, 8], 10))


class TestFuzzRow(unittest.TestCase):
    def test_fuzz_row_works_for_empty(self):
        self.assertEqual(fuzz_row([]), [])

    def test_fuzz_row_works_0(self):
        self.assertEqual(fuzz_row([1]), [1])

    def test_fuzz_row_works_1(self):
        self.assertEqual(fuzz_row([0]), [0])

    def test_fuzz_row_works_2(self):
        self.assertEqual(fuzz_row([1, 1]), [1, 1])

    def test_fuzz_row_works_3(self):
        self.assertEqual(fuzz_row([1, 0, 1]), [1, 0, 1])

    def test_fuzz_row_works_4(self):
        self.assertEqual(fuzz_row([1, 0, 1, 0]), [0, 1, 0, 1])

    def test_fuzz_row_works_5(self):
        self.assertEqual(fuzz_row([0, 1, 0, 1]), [0, 1, 0, 1])

    def test_fuzz_row_works_6(self):
        self.assertEqual(fuzz_row([0, 1, 0, 1, 0, 0]), [0, 0, 1, 0, 1, 0])

    def test_fuzz_row_works_7(self):
        self.assertEqual(fuzz_row([0, 0, 1, 0, 1, 0]), [0, 0, 0, 1, 0, 1])

    def test_fuzz_row_works_8(self):
        self.assertEqual(fuzz_row([0, 0, 0, 1, 0, 1]), [0, 0, 0, 1, 0, 1])

    def test_fuzz_row_works_9(self):
        self.assertEqual(fuzz_row([0, 0, 0, 1, 1, 1, 0, 0, 0, 0]), [0, 0, 0, 0, 1, 1, 1, 0, 0, 0])


class TestRowBlockConversion(unittest.TestCase):
    def test_toBlock_toRow0(self):
        self.assertEqual([1, 1, 1, 1, 1, 1, 1], to_row(to_block([1, 1, 1, 1, 1, 1, 1])))

    def test_toBlock_toRow1(self):
        self.assertEqual([1, 2, 1, 1, 1, 1, 1], to_row(to_block([1, 2, 1, 1, 1, 1, 1])))

    def test_toBlock_toRow2(self):
        self.assertEqual([1, 2, 1, 1, 1, 1, 2], to_row(to_block([1, 2, 1, 1, 1, 1, 2])))

    def test_toBlock_toRow3(self):
        self.assertEqual([1, 2, 2, 1, 1, 1, 2], to_row(to_block([1, 2, 2, 1, 1, 1, 2])))

    def test_toBlock_toRow4(self):
        self.assertEqual([1, 2, 2, 2, 1, 1, 2], to_row(to_block([1, 2, 2, 2, 1, 1, 2])))


class TestBlockConversion(unittest.TestCase):

    def test_toBlock_empty(self):
        self.assertEqual([], to_block([]))

    def test_toBlock_2_size(self):
        self.assertEqual([1, 0], to_block([1, 2]))

    def test_toBlock_3_size(self):
        self.assertEqual([1, 0, 0], to_block([1, 2, 2]))

    def test_toBlock_3_size_DifferentOrder(self):
        self.assertEqual([2, 0], to_block([1, 1, 2]))

    def test_toBlock_5_size_2_blocks(self):
        self.assertEqual([2, 0, 0, 0, 3], to_block([1, 1, 2, 2, 2, 1, 1, 1]))

    def test_toBlock_10_size_3_blocks(self):
        self.assertEqual([3, 0, 0, 0, 2, 0, 1], to_block([1, 1, 1, 2, 2, 2, 1, 1, 2, 1]))

    def test_toRow_empty(self):
        self.assertEqual([], to_row([]))

    def test_toRow_1(self):
        self.assertEqual([1], to_row([1]))

    def test_toRow_2_simple(self):
        self.assertEqual([1, 1], to_row([2]))

    def test_toRow_10_row_2_blocks(self):
        self.assertEqual([1, 1, 2, 2, 1, 1, 1, 1, 2, 2], to_row([2, 0, 0, 4, 0, 0]))


class TestCreateBasicRow(unittest.TestCase):

    def test_emptyRow_returnsEmpty(self):
        self.assertEqual([], starting_row([], 0))

    def test_1Row(self):
        self.assertEqual([1], starting_row([1], 1))

    def test_2Row(self):
        self.assertEqual([1, 2], starting_row([1], 2))

    def test_3Row(self):
        self.assertEqual([1, 2, 2], starting_row([1], 3))

    def test_4Row(self):
        self.assertEqual([1, 2, 1, 1], starting_row([1, 2], 4))

    def test_4Row_manyEmpty(self):
        self.assertEqual([1, 2, 2, 2], starting_row([1], 4))

    def test_10Row_2input_0(self):
        self.assertEqual([1, 2, 1, 1, 2, 2, 2, 2, 2, 2], starting_row([1, 2], 10))

    def test_10Row_2input_1(self):
        self.assertEqual([1, 1, 1, 2, 1, 1, 1, 1, 1, 2], starting_row([3, 5], 10))

    def test_10Row_2input_2(self):
        self.assertEqual([1, 1, 1, 1, 1, 2, 1, 1, 1, 1], starting_row([5, 4], 10))

    def test_10Row_3input_0(self):
        self.assertEqual([1, 1, 2, 1, 1, 2, 1, 1, 2, 2], starting_row([2, 2, 2], 10))

    def test_10Row_3input_1(self):
        self.assertEqual([1, 1, 1, 2, 1, 1, 1, 2, 1, 1], starting_row([3, 3, 2], 10))

    def test_20Row_lotsOfEmpty(self):
        self.assertEqual([1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2], starting_row([1], 20))
