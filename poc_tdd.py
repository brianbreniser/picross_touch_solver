#!/usr/bin/env python3

import unittest
from math import pow
import sys

# intify a list of characters of integers
def intify_list(l: list):
    return [int(i) for i in l]

# stringify a list of integers
def stringify_list(l: list):
    return [str(i) for i in l]


# (10, 10) -> [2, 2, 2, 2, 2, 2, 1, 2, 1, 2]
# (0, 10) -> [2, 2, 2, 2, 2, 2, 2, 2, 2, 2]
def get_bits_from_integer(num: int, size: int) -> list:
    # num: the number we will convert to an array of bits of
    # size: the padding for the array, to ensure correct size

    # Get the list, of the string, of the binary of number
    # adjust the string to size+2 characters (append with '0')
    # drop off the 0b (that pre-pends binary after converting from string)
    return list(str(bin(num))[2:].rjust(size, '0').replace('0', '2'))


# (10) -> [[2, 2, 2, 2, 2, 2, 2, 2, 2, 2], ..., [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]]
def get_array_of_all_possible_patterns(size: int) -> list:
    if size == 0: return [[]]
    return [get_bits_from_integer(i, size) for i in range(0, int(pow(2, size)))]


# ['1', '1', '2', '1', '1'] -> [2, 2]
def get_answer(pattern: list) -> list:
    pattern = stringify_list(pattern)
    pattern_string = ''.join(pattern)
    pattern_list = pattern_string.split('2')
    answer = []

    for j in pattern_list:
        answer.append(len(j))

    # remove all 0 items, so [2, 0, 2] would become [2, 2]
    answer = list(filter(lambda k: k != 0, answer))

    # Sometimes there is nothing in the pattern, we end up with []
    # So, just force a [0] to return
    if len(answer) == 0:
        answer = [0]

    return answer

# ([2, 7], [1, 1, 2, 1, 1, 1, 1, 1, 1, 1]) -> True
# ([2, 7], [1, 1, 2, 2, 1, 1, 1, 1, 1, 1]) -> False
def is_viable_pattern(answer: list, pattern: list) -> bool:
    patterns_answer = get_answer(pattern)
    return answer == patterns_answer

# ([1, 2], [1, 0]) -> True
# ([1, 1], [1, 0]) -> True
# ([2, 1], [1, 0]) -> False
# ([1, 2], [0, 0]) -> True
def is_aligned_with_proven(pattern: list, proven_pattern: list) -> bool:
    for i, element in enumerate(proven_pattern):
        if element != 0:
            if int(pattern[i]) != int(element):
                return False
    return True

# ([1, 2], [1, 2]) -> [1, 2]
# ([1, 2], [1, 1]) -> [1, 0]
# ([1, 1], [2, 2]) -> [0, 0]
def get_common_pattern(fuzzed_rows: list) -> list:
    if len(fuzzed_rows) == 0: return []
    common_row = []

    common_row = fuzzed_rows[0].copy()

    for row in fuzzed_rows:
        for i, element in enumerate(row):
            if element != common_row[i]:
                common_row[i] = 0

    return common_row

# ([8], 10) -> [0, 0, 1, 1, 1, 1, 1, 1, 0, 0]
def find_proveable_pattern(answers: list, proven_pattern: list, size: int) -> list:
    all_patterns = get_array_of_all_possible_patterns(size)
    viable_patterns = list(filter(lambda x: is_viable_pattern(answers, x), all_patterns))
    viable_and_aligned_with_previously_proven_patterns = list(filter(lambda y: is_aligned_with_proven(y, proven_pattern), viable_patterns))
    new_proven_pattern = get_common_pattern(viable_and_aligned_with_previously_proven_patterns)

    return intify_list(new_proven_pattern)


def has_zeros(matrix: list) -> bool:
    for i in matrix:
        for j in i:
            if j == 0:
                return True
    return False



def print_matrix(matrix: list):
    for i in matrix:
        print(i)

# Get the row and column scores for a matrix of points
def get_answers(rows: list) -> list:
    # Get a list of lists of columns from the rows
    columns = list(zip(*rows))
    row_scores = []
    column_scores = []

    # Score the rows
    for row in rows:
        row_scores.append(get_answer(row))

    # Score the columns
    for column in columns:
        column_scores.append(get_answer(column))

    return row_scores, column_scores

# Return the list where there are no empty string items
# ['', '1', '2'] -> ['1', '2']
def filter_empty(l: list):
    def is_not_empty_string(x: str):
        return x != ''
    return list(filter(lambda x: is_not_empty_string(x), l))


def print_state(matrix: list, row_answers: list, col_answers: list):
    just = 12
    print("".ljust(just), end="")
    for i in col_answers:
        print(str(i).ljust(just), end="")
    print()
    for i in range(0, len(matrix)):
        print(str(row_answers[i]).ljust(just), end="")
        for j in matrix[i]:
            if j == 0:
                print(str(" ").ljust(just), end="")
            elif j == 1:
                print(str("X").ljust(just), end="")
            elif j == 2:
                print(str(".").ljust(just), end="")
            else:
                print(str(j).ljust(just), end="")
        print()


def pretty_print_matrix(matrix: list):
    just = 2
    print()
    for i in range(0, len(matrix)):
        for j in matrix[i]:
            if j == 0:
                print(str(" ").ljust(just), end="")
            elif j == 1:
                print(str("X").ljust(just), end="")
            elif j == 2:
                print(str(" ").ljust(just), end="")
            else:
                print(str(j).ljust(just), end="")
        print()


# Print the answers given that the list of answers looks like: [[row, row], [column, column]]
# Will print the result of the get_answers() function
def print_answers(answers: list):
    print("             ", end="")

    # Print columns first
    for i in answers[1]:
        print(i, end=" ")

    print("")

    # Print rows second
    for i in answers[0]:
        print(i)

def main():
    print("It's working!")

    matrix_of_patterns = [
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        ]

    args = sys.argv[1].split(';')

    col = args[0]
    row = args[1]

    col = col.split(',')
    col_answers = [x.split(' ') for x in col]
    col_answers = [filter_empty(x) for x in col_answers]
    col_answers = [intify_list(x) for x in col_answers]

    row = row.split(',')
    row_answers = [x.split(' ') for x in row]
    row_answers = [filter_empty(x) for x in row_answers]
    row_answers = [intify_list(x) for x in row_answers]

    print("===================== row")
    print(row_answers)
    print("===================== col")
    print(col_answers)

    print()
    print("start:")
    print_state(matrix_of_patterns, row_answers, col_answers)

    while has_zeros(matrix_of_patterns):
        for i in range(0, len(matrix_of_patterns)):
            matrix_of_patterns[i] = find_proveable_pattern(row_answers[i], matrix_of_patterns[i], len(matrix_of_patterns))

        print()
        print("after row:")
        print_state(matrix_of_patterns, row_answers, col_answers)

        matrix_of_patterns = list(zip(*matrix_of_patterns)) # Rotates matrix

        for i in range(0, len(matrix_of_patterns)):
            matrix_of_patterns[i] = find_proveable_pattern(col_answers[i], matrix_of_patterns[i], len(matrix_of_patterns))

        matrix_of_patterns = list(zip(*matrix_of_patterns)) # Rotates matrix

        print()
        print("after columns:")
        print_state(matrix_of_patterns, row_answers, col_answers)

    print()
    print("Final:")
    print_state(matrix_of_patterns, row_answers, col_answers)

    print()
    print("Get answers:")
    print_answers(get_answers(matrix_of_patterns))

    pretty_print_matrix(matrix_of_patterns)






if __name__ == '__main__':
    main()


## Test section ###############################################################################################


class Test_get_bits_from_integer(unittest.TestCase):
    def test_0(self):
        self.assertEquals(['2', '2', '2', '2', '2', '2', '2', '2', '2', '2'], get_bits_from_integer(0, 10))
    def test_1(self):
        self.assertEquals(['2', '2', '2', '2', '2', '2', '2', '2', '2', '1'], get_bits_from_integer(1, 10))
    def test_3(self):
        self.assertEquals(['2', '2', '2', '2', '2', '2', '2', '2', '1', '1'], get_bits_from_integer(3, 10))
    def test_12(self):
        self.assertEquals(['2', '2', '2', '2', '2', '2', '1', '2', '1', '2'], get_bits_from_integer(10, 10))
    def test_1023(self):
        self.assertEquals(['1', '1', '1', '1', '1', '1', '1', '1', '1', '1'], get_bits_from_integer(1023, 10))
    def test_0_5(self):
        self.assertEquals(['2', '2', '2', '2', '2'], get_bits_from_integer(0, 5))
    def test_0_5(self):
        self.assertEquals(['2', '2', '2', '2', '1'], get_bits_from_integer(1, 5))
    def test_0_5(self):
        self.assertEquals(['2', '1', '2', '1', '2'], get_bits_from_integer(10, 5))
    def test_0_5(self):
        self.assertEquals(['1', '1', '1', '1', '1'], get_bits_from_integer(int(pow(2, 5)) - 1, 5))


class Test_get_array_of_all_possible_patterns(unittest.TestCase):
    def test_0(self):
        self.assertEquals([[]], get_array_of_all_possible_patterns(0))

    def test_1(self):
        self.assertEquals([['2'], ['1']], get_array_of_all_possible_patterns(1))

    def test_2(self):
        self.assertEquals([
                           ['2', '2'],
                           ['2', '1'],
                           ['1', '2'],
                           ['1', '1'],
                          ], get_array_of_all_possible_patterns(2))

    def test_10_first(self):
        self.assertEquals(
                          ['2', '2', '2', '2', '2', '2', '2', '2', '2', '2'],
                          get_array_of_all_possible_patterns(10)[0])

    def test_10_last(self):
        self.assertEquals(
                          ['1', '1', '1', '1', '1', '1', '1', '1', '1', '1'],
                          get_array_of_all_possible_patterns(10)[-1])

    # def test_20_first(self): # slow
    #     self.assertEquals(
    #                       ['2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2'],
    #                       get_array_of_all_possible_patterns(20)[0])

    # def test_20_last(self): # slow
    #     self.assertEquals(
    #                       ['1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'],
    #                       get_array_of_all_possible_patterns(20)[-1])


class Test_get_answer(unittest.TestCase):
    def test_0(self):
        self.assertEquals([0], get_answer([]))
    def test_3(self):
        self.assertEquals([3], get_answer(['1', '1', '1']))
    def test_realistic(self):
        self.assertEquals([3, 5], get_answer(['1', '1', '1', '2', '2', '1', '1', '1', '1', '1']))


class Test_is_viable_pattern(unittest.TestCase):
    def test_00(self):
        self.assertEquals(True, is_viable_pattern([0], []))
    def test_0(self):
        self.assertEquals(False, is_viable_pattern([1], []))
    def test_1(self):
        self.assertEquals(True, is_viable_pattern([1], ['1']))
    def test_2_2(self):
        self.assertEquals(True, is_viable_pattern([2], ['1', '1']))
    def test_2_3(self):
        self.assertEquals(True, is_viable_pattern([2], ['1', '1', '2']))
    def test_2_3(self):
        self.assertEquals(True, is_viable_pattern([2], ['2', '1', '1', '2']))
    def test_2_10_0(self):
        self.assertEquals(True, is_viable_pattern([2], ['2', '1', '1', '2', '2', '2', '2', '2', '2', '2']))
    def test_2_10_1(self):
        self.assertEquals(True, is_viable_pattern([2], ['2', '2', '2', '2', '2', '1', '1', '2', '2', '2']))
    def test_22_10_0(self):
        self.assertEquals(True, is_viable_pattern([2, 2], ['2', '1', '1', '2', '2', '1', '1', '2', '2', '2']))
    def test_22_10_0_false(self):
        self.assertEquals(False, is_viable_pattern([2, 2], ['1', '1', '1', '2', '2', '1', '1', '2', '2', '2']))


class Test_is_aligned_with_proven(unittest.TestCase):
    def test_one_match_second_wildcard_2(self):
        self.assertEqual(True, is_aligned_with_proven([1, 2], [1, 0]))

    def test_one_match_second_wildcard_1(self):
        self.assertEqual(True, is_aligned_with_proven([1, 1], [1, 0]))

    def test_one_noMatch_second_wildcard_1(self):
        self.assertEqual(False, is_aligned_with_proven([2, 1], [1, 0]))

    def test_both_wildcard(self):
        self.assertEqual(True, is_aligned_with_proven([1, 2], [0, 0]))

    def test_first_wildcard_1(self):
        self.assertEqual(True, is_aligned_with_proven([1, 2], [0, 2]))

    def test_first_wildcard_2(self):
        self.assertEqual(True, is_aligned_with_proven([2, 2], [0, 2]))

    def test_no_wildcard_must_match_1(self):
        self.assertEqual(True, is_aligned_with_proven([1, 1], [1, 1]))

    def test_no_wildcard_must_match_2(self):
        self.assertEqual(False, is_aligned_with_proven([1, 2], [1, 1]))

    def test_no_wildcard_must_match_3(self):
        self.assertEqual(False, is_aligned_with_proven([2, 2], [1, 1]))

    def test_no_wildcard_must_match_4(self):
        self.assertEqual(False, is_aligned_with_proven([2, 1], [1, 1]))

    def test_must_match_5(self):
        self.assertEqual(False, is_aligned_with_proven([2, 2, 1, 1, 1, 1, 1, 1, 2, 2], [0, 0, 0, 0, 0, 0, 0, 0, 1, 0]))

    def test_must_match_6(self):
        self.assertEqual(True, is_aligned_with_proven([2, 1, 1, 1, 1, 1, 1, 1, 1, 2], [0, 0, 0, 0, 0, 0, 0, 0, 1, 0]))



class TestWhatIsInCommon(unittest.TestCase):
    def test_empty(self):
        self.assertEqual([], get_common_pattern([[]]))

    def test_emtpy_2(self):
        self.assertEqual([], get_common_pattern([[], []]))

    def test_emtpy_3(self):
        self.assertEqual([], get_common_pattern([[], [], []]))

    def test_fill_1(self):
        self.assertEqual([1], get_common_pattern([[1]]))

    def test_fill_2(self):
        self.assertEqual([1], get_common_pattern([[1], [1]]))

    def test_fill_3(self):
        self.assertEqual([1], get_common_pattern([[1], [1], [1]]))

    def test_diff_3(self):
        self.assertEqual([0], get_common_pattern([[1], [1], [2]]))

    def test_diff_3_2_0(self):
        self.assertEqual([0, 0], get_common_pattern([[1, 1], [1, 1], [2, 2]]))

    def test_diff_3_2_1(self):
        self.assertEqual([1, 2], get_common_pattern([[1, 2], [1, 2], [1, 2]]))

    def test_diff_3_2_2(self):
        self.assertEqual([1, 0], get_common_pattern([[1, 2], [1, 2], [1, 1]]))

    def test_10_specific(self):
        self.assertEqual([0, 0, 1, 1, 1, 0, 0, 0, 0, 0], get_common_pattern([
                         [1, 1, 1, 1, 1, 2, 1, 1, 2, 2],
                         [2, 1, 1, 1, 1, 1, 2, 1, 1, 2],
                         [2, 2, 1, 1, 1, 1, 1, 2, 1, 1]]))
                 # notice       1  1  1                   <-- that is what the commonality is


class Test_find_proveable_pattern(unittest.TestCase):

    # def test_emptyRow_returnsEmpty(self):
    #     self.assertEqual([], find_proveable_pattern([], [], 0))

    def test_1Row(self):
        self.assertEqual([1], find_proveable_pattern([1], [0], 1))

    def test_1Row_10(self):
        self.assertEqual([2, 2, 2, 2, 2, 2, 2, 2, 2, 2], find_proveable_pattern([0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 10))

    def test_3Row(self):
        self.assertEqual([1, 2, 1], find_proveable_pattern([1, 1], [0, 0, 0], 3))

    def test_5Row(self):
        self.assertEqual([1, 1, 2, 1, 1], find_proveable_pattern([2, 2], [0, 0, 0, 0, 0], 5))

    def test_10Row_3input_1(self):
        self.assertEqual([1, 1, 1, 2, 1, 1, 1, 2, 1, 1], find_proveable_pattern([3, 3, 2], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 10))

    def test_8_10(self):
        self.assertEqual([0, 0, 1, 1, 1, 1, 1, 1, 0, 0], find_proveable_pattern([8], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 10))

    def test_8_10_with_proven(self):
        self.assertEqual([2, 0, 1, 1, 1, 1, 1, 1, 1, 0], find_proveable_pattern([8], [0, 0, 0, 0, 0, 0, 0, 0, 1, 0], 10))

    def test_5_2_10_no_proven(self):
        self.assertEqual([0, 0, 1, 1, 1, 0, 0, 0, 0, 0], find_proveable_pattern([5, 2], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 10))

    def test_5_2_10_with_proven(self):
        self.assertEqual([1, 1, 1, 1, 1, 2, 0, 0, 0, 0], find_proveable_pattern([5, 2], [1, 0, 0, 0, 0, 0, 0, 0, 0, 0], 10))

    def test_5_3_with_proven(self):
        self.assertEqual([1, 1, 1, 1, 1, 2, 0, 1, 1, 0], find_proveable_pattern([5, 3], [1, 0, 0, 0, 0, 0, 0, 0, 0, 0], 10))
