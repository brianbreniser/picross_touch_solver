#!/usr/bin/env python3

import unittest
from math import pow
import sys
from concurrent import futures
import itertools

# intify a list of characters of integers
def intify_list(l: list):
    return [int(i) for i in l]

# stringify a list of integers
def stringify_list(l: list):
    return [str(i) for i in l]


# (10, 10) -> [2, 2, 2, 2, 2, 2, 1, 2, 1, 2]
# (0, 10) -> [2, 2, 2, 2, 2, 2, 2, 2, 2, 2]
def get_bits_from_integer(num: int, size: int) -> list:
    # num: the number we will convert to an array of bits of
    # size: the padding for the array, to ensure correct size

    # Get the list, of the string, of the binary of number
    # adjust the string to size+2 characters (append with '0')
    # drop off the 0b (that pre-pends binary after converting from string)
    return list(str(bin(num))[2:].rjust(size, '0').replace('0', '2'))


# (10) -> [[2, 2, 2, 2, 2, 2, 2, 2, 2, 2], ..., [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]]
def get_array_of_all_possible_patterns(size: int) -> list:
    if size == 0: return [[]]
    return [get_bits_from_integer(i, size) for i in range(0, int(pow(2, size)))]


# ['1', '1', '2', '1', '1'] -> [2, 2]
def get_answer(pattern: list) -> list:
    pattern = stringify_list(pattern)
    pattern_string = ''.join(pattern)
    pattern_list = pattern_string.split('2')
    answer = []

    for j in pattern_list:
        answer.append(len(j))

    # remove all 0 items, so [2, 0, 2] would become [2, 2]
    answer = list(filter(lambda k: k != 0, answer))

    # Sometimes there is nothing in the pattern, we end up with []
    # So, just force a [0] to return
    if len(answer) == 0:
        answer = [0]

    return answer


def print_matrix(matrix: list):
    for i in matrix:
        print(i)

# Get the row and column scores for a matrix of points
def get_answers(rows: list) -> list:
    # Get a list of lists of columns from the rows
    columns = list(zip(*rows))
    row_scores = []
    column_scores = []

    # Score the rows
    for row in rows:
        row_scores.append(get_answer(row))

    # Score the columns
    for column in columns:
        column_scores.append(get_answer(column))

    return row_scores, column_scores

# Return the list where there are no empty string items
# ['', '1', '2'] -> ['1', '2']
def filter_empty(l: list):
    def is_not_empty_string(x: str):
        return x != ''
    return list(filter(lambda x: is_not_empty_string(x), l))


def print_state(matrix: list, row_answers: list, col_answers: list):
    just = 12
    print("".ljust(just), end="")
    for i in col_answers:
        print(str(i).ljust(just), end="")
    print()
    for i in range(0, len(matrix)):
        print(str(row_answers[i]).ljust(just), end="")
        for j in matrix[i]:
            if j == 0:
                print(str(" ").ljust(just), end="")
            elif j == 1:
                print(str("X").ljust(just), end="")
            elif j == 2:
                print(str(".").ljust(just), end="")
            else:
                print(str(j).ljust(just), end="")
        print()


def pretty_print_matrix(matrix: list):
    just = 2
    print()
    for i in range(0, len(matrix)):
        for j in matrix[i]:
            if j == 0:
                print(str(" ").ljust(just), end="")
            elif j == 1:
                print(str("X").ljust(just), end="")
            elif j == 2:
                print(str(" ").ljust(just), end="")
            else:
                print(str(j).ljust(just), end="")
        print()


# Print the answers given that the list of answers looks like: [[row, row], [column, column]]
# Will print the result of the get_answers() function
def print_answers(answers: list):
    print("             ", end="")

    # Print columns first
    for i in answers[1]:
        print(i, end=" ")

    print("")

    # Print rows second
    for i in answers[0]:
        print(i)

def main():
    print("It's working!")

    args = sys.argv[1].split(';')

    col = args[0]
    row = args[1]

    col = col.split(',')
    col_answers = [x.split(' ') for x in col]
    col_answers = [filter_empty(x) for x in col_answers]
    col_answers = [intify_list(x) for x in col_answers]

    row = row.split(',')
    row_answers = [x.split(' ') for x in row]
    row_answers = [filter_empty(x) for x in row_answers]
    row_answers = [intify_list(x) for x in row_answers]

    print("===================== row")
    print(row_answers)
    print("===================== col")
    print(col_answers)

    def convert_to_10_10(l: list) -> list:
        new_list = []
        for i in range(0, 100, 10):
            new_list.append(l[i:i+9])
        return new_list

    def rotate_matrix(l: list) -> list:
        return list(zip(*l)) # Rotates matrix

    def answers_equal(left: list, right: list) -> bool:
        left_row = left[0]
        left_col = left[1]

        right_row = right[0]
        right_col = right[1]

        return left_row == right_row and left_col == right_col 

    # def do_work():
    for i in range(0, int(pow(2, 100))):
        bits = get_bits_from_integer(i, 100)
        matrix = convert_to_10_10(bits)
        answers = get_answers(matrix)

        if (i % 10000 == 0):
            print("tried up to: " + str(i))

        if answers_equal(answers, [row_answers, col_answers]):
            print("Equal!!")
            print("############################################## Ended loop with answers:")
            print(i)
            print_answers(answers)
            print_answers([row_answers, col_answers])
            pretty_print_matrix(matrix)

    # executor = futures.ProcessPoolExecutor(10)
    # future = [executor.submit(do_work, i) for i in range(0, int(pow(2, 100)))]
    # concurrent.futures.wait(future)


if __name__ == '__main__':
    main()
