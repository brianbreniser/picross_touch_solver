#!/usr/bin/env python3

import unittest
from math import pow
from utilities import intify_list, get_answers, print_answers, get_row_answer_split_on_2


##
# New experimentation section, will work on a brute-force method first
##

# (10, 10) -> [2, 2, 2, 2, 2, 2, 1, 2, 1, 2]
# (0, 10) -> [2, 2, 2, 2, 2, 2, 2, 2, 2, 2]
def get_bits_from_integer(num: int, size: int) -> list:
    # num: the number we will convert to an array of bits of
    # size: the padding for the array, to ensure correct size

    # Get the list, of the string, of the binary of number
    # adjust the string to size+2 characters (append with '0')
    # drop off the 0b (that pre-pends binary after converting from string)
    return list(str(bin(num))[2:].rjust(size, '0').replace('0', '2'))

# (10) -> [[2, 2, 2, 2, 2, 2, 2, 2, 2, 2], ..., [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]]
def get_array_of_all_possible_patterns(size: int) -> list:
    if size == 0: return [['2', '2', '2', '2', '2', '2', '2', '2', '2', '2']]
    return [get_bits_from_integer(i, size) for i in range(0, int(pow(2, size)))]

# ([2, 7], [1, 1, 2, 1, 1, 1, 1, 1, 1, 1]) -> True
# ([2, 7], [1, 1, 2, 2, 1, 1, 1, 1, 1, 1]) -> False
def is_viable_pattern(answer: list, pattern: list) -> bool:
    pattern_answer = get_row_answer_split_on_2(pattern)
    return answer == pattern_answer

# ([1, 2], [1, 2]) -> [1, 2]
# ([1, 2], [1, 1]) -> [1, 0]
# ([1, 1], [2, 2]) -> [0, 0]
def get_common_pattern(fuzzed_rows: list) -> list:
    if len(fuzzed_rows) == 0: return []
    common_row = []

    common_row = fuzzed_rows[0].copy()

    for row in fuzzed_rows:
        for i, element in enumerate(row):
            if element != common_row[i]:
                common_row[i] = 0

    return common_row

# ([8], 10) -> [0, 0, 1, 1, 1, 1, 1, 1, 0, 0]
def find_proveable_pattern(answers: list, proven_pattern: list, size: int) -> list:
    get_all_options = get_array_of_all_possible_patterns(size)
    viable_array_of_options = list(filter(lambda x: is_viable_pattern(answers, x), get_all_options))

    viable_and_proven_array_of_options = list(filter(lambda y: is_aligned_with_proven(y, proven_pattern), viable_array_of_options))

    common_pattern = get_common_pattern(viable_and_proven_array_of_options)

    return intify_list(common_pattern)

# ([1, 2], [1, 0]) -> True
# ([1, 1], [1, 0]) -> True
# ([2, 1], [1, 0]) -> False
# ([1, 2], [0, 0]) -> True
def is_aligned_with_proven(pattern: list, proven_pattern: list) -> bool:
    for i, element in enumerate(proven_pattern):
        if element != 0:
            if int(pattern[i]) != int(element):
                return False
    return True


def print_matrix(matrix: list):
    for i in matrix:
        print(i)

def has_zeros(matrix: list) -> bool:
    for i in matrix:
        for j in i:
            if j == 0:
                return True
    return False


##
# Main application
##


def main():
    matrix_of_patterns = [
        [0, 0],
        [0, 0],
        ]

    print("start:")
    print_matrix(matrix_of_patterns)

    row_answers = [
        [0],
        [2],
        ]

    col_answers = [
        [1],
        [1],
        ]

    while has_zeros(matrix_of_patterns):
        for i in range(0, len(matrix_of_patterns)):
            matrix_of_patterns[i] = find_proveable_pattern(row_answers[i], matrix_of_patterns[i], len(matrix_of_patterns))

        print()
        print("after row:")
        print_matrix(matrix_of_patterns)

        matrix_of_patterns = list(zip(*matrix_of_patterns)) # Rotates matrix

        for i in range(0, len(matrix_of_patterns)):
            matrix_of_patterns[i] = find_proveable_pattern(col_answers[i], matrix_of_patterns[i], len(matrix_of_patterns))

        matrix_of_patterns = list(zip(*matrix_of_patterns)) # Rotates matrix

        print()
        print("after columns:")
        print_matrix(matrix_of_patterns)

    print("Final:")
    print_matrix(matrix_of_patterns)

    print("Get answers:")
    print_answers(get_answers(matrix_of_patterns))

if __name__ == '__main__':
    main()



##
# Test section
##



class Test_find_proveable_pattern(unittest.TestCase):

    # def test_emptyRow_returnsEmpty(self):
    #     self.assertEqual([], find_proveable_pattern([], [], 0))

    def test_1Row(self):
        self.assertEqual([1], find_proveable_pattern([1], [0], 1))

    def test_1Row_10(self):
        self.assertEqual([2, 2, 2, 2, 2, 2, 2, 2, 2, 2], find_proveable_pattern([0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 10))

    def test_3Row(self):
        self.assertEqual([1, 2, 1], find_proveable_pattern([1, 1], [0, 0, 0], 3))

    def test_5Row(self):
        self.assertEqual([1, 1, 2, 1, 1], find_proveable_pattern([2, 2], [0, 0, 0, 0, 0], 5))

    def test_10Row_3input_1(self):
        self.assertEqual([1, 1, 1, 2, 1, 1, 1, 2, 1, 1], find_proveable_pattern([3, 3, 2], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 10))

    def test_8_10(self):
        self.assertEqual([0, 0, 1, 1, 1, 1, 1, 1, 0, 0], find_proveable_pattern([8], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 10))

    def test_8_10_with_proven(self):
        self.assertEqual([2, 0, 1, 1, 1, 1, 1, 1, 1, 0], find_proveable_pattern([8], [0, 0, 0, 0, 0, 0, 0, 0, 1, 0], 10))

    def test_5_2_10_no_proven(self):
        self.assertEqual([0, 0, 1, 1, 1, 0, 0, 0, 0, 0], find_proveable_pattern([5, 2], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 10))

    def test_5_2_10_with_proven(self):
        self.assertEqual([1, 1, 1, 1, 1, 2, 0, 0, 0, 0], find_proveable_pattern([5, 2], [1, 0, 0, 0, 0, 0, 0, 0, 0, 0], 10))

    def test_5_3_with_proven(self):
        self.assertEqual([1, 1, 1, 1, 1, 2, 0, 1, 1, 0], find_proveable_pattern([5, 3], [1, 0, 0, 0, 0, 0, 0, 0, 0, 0], 10))

class Test_is_aligned_with_proven(unittest.TestCase):
    def test_one_match_second_wildcard_2(self):
        self.assertEqual(True, is_aligned_with_proven([1, 2], [1, 0]))

    def test_one_match_second_wildcard_1(self):
        self.assertEqual(True, is_aligned_with_proven([1, 1], [1, 0]))

    def test_one_noMatch_second_wildcard_1(self):
        self.assertEqual(False, is_aligned_with_proven([2, 1], [1, 0]))

    def test_both_wildcard(self):
        self.assertEqual(True, is_aligned_with_proven([1, 2], [0, 0]))

    def test_first_wildcard_1(self):
        self.assertEqual(True, is_aligned_with_proven([1, 2], [0, 2]))

    def test_first_wildcard_2(self):
        self.assertEqual(True, is_aligned_with_proven([2, 2], [0, 2]))

    def test_no_wildcard_must_match_1(self):
        self.assertEqual(True, is_aligned_with_proven([1, 1], [1, 1]))

    def test_no_wildcard_must_match_2(self):
        self.assertEqual(False, is_aligned_with_proven([1, 2], [1, 1]))

    def test_no_wildcard_must_match_3(self):
        self.assertEqual(False, is_aligned_with_proven([2, 2], [1, 1]))

    def test_no_wildcard_must_match_4(self):
        self.assertEqual(False, is_aligned_with_proven([2, 1], [1, 1]))

    def test_no_wildcard_must_match_5(self):
        self.assertEqual(False, is_aligned_with_proven([2, 2, 1, 1, 1, 1, 1, 1, 2, 2], [0, 0, 0, 0, 0, 0, 0, 0, 1, 0]))

    def test_no_wildcard_must_match_6(self):
        self.assertEqual(True, is_aligned_with_proven([2, 1, 1, 1, 1, 1, 1, 1, 1, 2], [0, 0, 0, 0, 0, 0, 0, 0, 1, 0]))



class Test_get_bits_from_integer(unittest.TestCase):
    def test_0(self):
        self.assertEquals(['2', '2', '2', '2', '2', '2', '2', '2', '2', '2'], get_bits_from_integer(0, 10))
    def test_1(self):
        self.assertEquals(['2', '2', '2', '2', '2', '2', '2', '2', '2', '1'], get_bits_from_integer(1, 10))
    def test_3(self):
        self.assertEquals(['2', '2', '2', '2', '2', '2', '2', '2', '1', '1'], get_bits_from_integer(3, 10))
    def test_12(self):
        self.assertEquals(['2', '2', '2', '2', '2', '2', '1', '2', '1', '2'], get_bits_from_integer(10, 10))
    def test_1023(self):
        self.assertEquals(['1', '1', '1', '1', '1', '1', '1', '1', '1', '1'], get_bits_from_integer(1023, 10))


class Test_get_array_of_all_possible_patterns(unittest.TestCase):
    def test_0(self):
        self.assertEquals([['2', '2', '2', '2', '2', '2', '2', '2', '2', '2']], get_array_of_all_possible_patterns(0))

    # def test_1(self):
    #     self.assertEquals([['2', '2', '2', '2', '2', '2', '2', '2', '2', '2'],
    #                        ['2', '2', '2', '2', '2', '2', '2', '2', '2', '1']], get_array_of_all_possible_patterns(1))

    # def test_2(self):
    #     self.assertEquals([['2', '2', '2', '2', '2', '2', '2', '2', '2', '2'],
    #                        ['2', '2', '2', '2', '2', '2', '2', '2', '2', '1'],
    #                        ['2', '2', '2', '2', '2', '2', '2', '2', '1', '2'],
    #                        ['2', '2', '2', '2', '2', '2', '2', '2', '1', '1']], get_array_of_all_possible_patterns(2))


class TestWhatIsInCommon(unittest.TestCase):
    def test_empty(self):
        self.assertEqual([], get_common_pattern([[]]))

    def test_emtpy_2(self):
        self.assertEqual([], get_common_pattern([[], []]))

    def test_emtpy_3(self):
        self.assertEqual([], get_common_pattern([[], [], []]))

    def test_fill_1(self):
        self.assertEqual([1], get_common_pattern([[1]]))

    def test_fill_2(self):
        self.assertEqual([1], get_common_pattern([[1], [1]]))

    def test_fill_3(self):
        self.assertEqual([1], get_common_pattern([[1], [1], [1]]))

    def test_diff_3(self):
        self.assertEqual([0], get_common_pattern([[1], [1], [2]]))

    def test_diff_3_2_0(self):
        self.assertEqual([0, 0], get_common_pattern([[1, 1], [1, 1], [2, 2]]))

    def test_diff_3_2_1(self):
        self.assertEqual([1, 2], get_common_pattern([[1, 2], [1, 2], [1, 2]]))

    def test_diff_3_2_2(self):
        self.assertEqual([1, 0], get_common_pattern([[1, 2], [1, 2], [1, 1]]))

    def test_10_specific(self):
        self.assertEqual([0, 0, 1, 1, 1, 0, 0, 0, 0, 0], get_common_pattern([
            [1, 1, 1, 1, 1, 2, 1, 1, 2, 2],
            [2, 1, 1, 1, 1, 1, 2, 1, 1, 2],
            [2, 2, 1, 1, 1, 1, 1, 2, 1, 1]]))
    # notice       1  1  1                   <-- that is what the commonality is
